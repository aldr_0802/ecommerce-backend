/* 

NOTE: We will use nodemailer to inform the user that his order has been confirmed and is going to be delievered
While we are going to use dotenv dependency to hide some confidential info like our MongoDB Atlas account username and password

*/
const express=require('express')
const mongoose=require('mongoose')
const nodemailer=require('nodemailer')
const cors=require('cors')
const { OAuth2Client } = require('google-auth-library')
require('dotenv').config()
const auth=require('./auth')
const clientId="572311522502-le83vksl2pnmp7k6rjkkeht3e7qgjbsf.apps.googleusercontent.com"

let app=express()

app.use(express.json())
app.use(cors())
/* DB CONNECTION */

mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true}, 
	function(){ console.log(`Connected to DB`)})
	
/* SCHEMA & MODEL */	
	
let UserProfile=mongoose.model('USER PROFILES', new mongoose.Schema({
	firstName: { 
		type: String
	}, lastName: { 
		type: String
	}, address: { 
		type: String
	}, email: { 
		type: String, 
		required: true
	}, password: { 
		type: String
	}, isAdmin: {
		type: Boolean,
		default: false
	},loginType: {
		type: String
	},cart: [{ 
		productId: { 
			type: String
		}, product_title: {
			type: String
		}, product_main_image_url: {
			type: String
		}, app_sale_price: {
			type: String
		}
	}], purchasedProduct: [{ 
		productId: { 
			type: String
		}, product_title: {
			type: String
		}, product_main_image_url: {
			type: String
		}, app_sale_price: {
			type: String
		}, status: { 
			type: String, 
			default: `PENDING`
		}
	}]
}))

let AllOrders=mongoose.model('ORDERS', new mongoose.Schema({
	status: {
		type: String,
		default: `RECEIVED`
	},productId: {
		type: String
	}, userId: {
		type: String
	}, product_title: {
		type: String
	}, product_main_image_url: {
		type: String
	}, app_sale_price: {
		type: String
	}, orderId: {
		type: String
	}, buyerProfile: {
		type: String
	}
}))


/* ROUTES 
	1. Register
	2. Login
	3. Add to cart
	4. Checkout product
*/


app.post('/register', async function(req, res){
	
	let NewUserProfile=new UserProfile({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		address: req.body.address,
		email: req.body.email,
		password: req.body.password,
		loginType: 'email'
	})
	
	res.send(await NewUserProfile.save())
})


app.post('/login', async function(req, res){
	let User=await UserProfile.findOne({email: req.body.email})
	if(User.password==req.body.password){
		res.send({
			'_id': User._id,
			'firstName': User.firstName,
			'lastName': User.lastName,
			'email': User.email,
			'isAdmin': User.isAdmin
		})
	} else {
		res.send(`USER CANT BE FOUND`)
	}
})

app.post('/user-details', async function(req, res){
	let UserDetails=await UserProfile.findById(req.body.userId)
	res.send(UserDetails)
})

app.post('/add-to-cart', async function(req, res){
	/*
	NOTE
	1.Find the user information using the access token saved at localStorage
	2.If a user is found, push the productId into the user's cart which is is an array of objects
	*/
	let FoundUser=await UserProfile.findById(req.body.userId)
	
	if(FoundUser){
		
		FoundUser.cart.push({
			productId: req.body.productId,
			product_title: req.body.product_title,
			product_main_image_url: req.body.product_main_image_url, 
			app_sale_price: req.body.app_sale_price
		})
		
		res.send(await FoundUser.save())
	}
})

/*
NOTE
1.Push to purchasedProduct once the user opt to checkout the product
2.Remove the productId from cart
*/
app.post('/checkout-product', async function(req, res){
	
	let FoundUser=await UserProfile.findById(req.body.userId)
	
	if(FoundUser){
		
		FoundUser.cart.forEach(index=> {
			if(index.productId==req.body.productId){
				index.remove()
			}
		})
		console.log(FoundUser.cart.length)
		
		FoundUser.purchasedProduct.push({
			productId: req.body.productId,
			product_title: req.body.product_title,
			product_main_image_url: req.body.product_main_image_url, 
			app_sale_price: req.body.app_sale_price
		})
		
			const transporter=nodemailer.createTransport({
				host: 'smtp.gmail.com',
				port: 465,
				secure: true, 
				auth: {
					user: 'nexseedcebu12345@gmail.com',
					pass: 'createsoftware'
				}
			})
			
			transporter.sendMail({
				from: 'donotreply@gmail.com',
				to: FoundUser.email,
				subject: 'To be delivered',
				text: `Your order is going to be delivered`,
				html: `
				<h1>Thank you for shopping at NEXSTORE!</h1>
				<p>Click the link below upon receiving your product</p>
				<a href='https://nexstorewebapp.vercel.app/userProfile'>Click here</a>
				`
				
			})
		
		res.send(await FoundUser.save())
	}
	
	
})

app.post('/paypal', async function(req, res){
	let FoundUser=await UserProfile.findById(req.body.userId)
	
	if(FoundUser){
		FoundUser.cart.splice(0,FoundUser.cart.length)
	}
	res.send(await FoundUser.save())
})


app.post('/google-log-in', async function(req, res){
	const client=new OAuth2Client(clientId)
	const data=await client.verifyIdToken({
		idToken: req.body.tokenId,
		audience: clientId
	})
	
	console.log(data)
	
	let EmailAlreadyAtUse=await UserProfile.findOne({email: data.payload.email})
	
	if(EmailAlreadyAtUse){
		/* 
		NOTE : INITIAL CODE 
		res.send('EMAIL ALREADY AT USE')
		*/
		
		res.send({
			'_id': EmailAlreadyAtUse._id,
			'firstName': EmailAlreadyAtUse.firstName,
			'lastName': EmailAlreadyAtUse.lastName,
			'email': EmailAlreadyAtUse.email,
			'isAdmin': EmailAlreadyAtUse.isAdmin
		})
		
	} else {
		
		let NewUserProfile=new UserProfile({
			firstName: data.payload.given_name,
			lastName: data.payload.family_name,
			email: data.payload.email,
			loginType: 'google'
		})
		
		console.log(await NewUserProfile.save())
	}
	

})


app.post('/delete-from-cart', async function(req, res){
	let UsersCart=await UserProfile.findById(req.body.userId)
	if(UsersCart){
		UsersCart.cart.forEach(index=> {
			//console.log(index)
			if(index.productId==req.body.productId){
				index.remove()
			}
		})
		
		res.send(await UsersCart.save())
	}
})


//ROUTE OF BUYER
app.post('/product-received', async function(req, res){
	let UserFound=await UserProfile.findById(req.body.userId)
	
	if(UserFound && (req.body.productId)!== ''){
		UserFound.purchasedProduct.map(items=> {
			if(items.productId==req.body.productId){
				items.status='RECEIVED'
			}
		})
		
		/*
		NOTE : Check its functionality for tomorrow since its not pushing the objects
		Reserved activity for March 15, 2021
		*/
		let ReceivedOrder=new AllOrders({
			orderId: req.body.orderId,
			productId: req.body.productId,
			userId: req.body.userId,
			product_title: req.body.product_title,
			product_main_image_url: req.body.product_main_image_url,
			app_sale_price: req.body.app_sale_price
		})
		
		return (await UserFound.save() && await ReceivedOrder.save()) ? res.send('ORDERS RECEIVED') : res.send('ORDERS NOT RECEIVED')
		
	}
})

app.post('/update-profile', async function(req, res){
	let FoundUser=await UserProfile.findById(req.body.userId)
	
	if(FoundUser){
		FoundUser.firstName=req.body.firstName
		FoundUser.lastName=req.body.lastName
		FoundUser.address=req.body.address
		
		res.send(await FoundUser.save())
	}
})


/* NOTE: GETS ALL PURCHASED PRODUCTS BY CUSTOMER */
app.get('/get-all-orders', async function(req, res){
	res.send(await AllOrders.find())
})


app.get('/all-users', async function(req, res){
	res.send(await UserProfile.find())
})
app.listen(process.env.PORT || 4000, function() { console.log(`Server running at port 4000`)})

