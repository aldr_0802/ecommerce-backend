/*
NOTE: This is the auth script
	For the create access token, we will be needing user data, a secret key and a time laspse
	Time lapse how long the token is going to valid
*/
const jsonwebtoken=require('jsonwebtoken')
let secret='secretkey'

module.exports.createAccessToken=function(User){
	let UserData={
		userId: User._id,
		isAdmin: User.isAdmin,
		email: User.email
	}
	return jsonwebtoken.sign(UserData, secret, {})
}

/*
	VERIFY 
*/
module.exports.verify=function(request, response, next){
	let token=request.headers.authorization
	token=token.slice(7, token.length)
	if(token){
		return jsonwebtoken.verify(token, secret) ? next() : response.send(`AUTH FAILED`)
	}else{
		return `NO TOKEN FOUND`
	}
}

module.exports.decode = function(token){
	
	if(token) {
		token = token.slice(7, token.length)
		if(jsonwebtoken.verify(token, secret)) { 
			return jsonwebtoken.decode(token, {complete: true}).payload
		} else {
			return `AUTH FAILED`
		}
	} else {
		return `NO TOKEN RETURNED`
	}
}
